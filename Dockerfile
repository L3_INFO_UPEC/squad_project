FROM ubuntu:20.04
RUN apt-get update -y && \ 
                apt-get install nginx=1.18.0* -y --no-install-recommends && \
                apt-get install php-fpm=2:7.4+75 -y --no-install-recommends && \
                rm -rf /var/lib/apt/lists/*
COPY start.sh /root/start.sh
RUN chmod +x /root/start.sh
COPY Website/ /var/www/html/
COPY conf/default /etc/nginx/sites-available/default
EXPOSE 80


CMD ["/root/start.sh"]